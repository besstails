# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-06-23 18:07+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: config/chroot_local-includes/etc/whisperback/config.py:64
#, python-format
msgid ""
"<h1>Help us fix your bug!</h1>\n"
"<p>Read <a href=\"%s\">our bug reporting instructions</a>.</p>\n"
"<p><strong>Do not include more personal information than\n"
"needed!</strong></p>\n"
"<h2>About giving us an email address</h2>\n"
"<p>If you don't mind disclosing some bits of your identity\n"
"to Tails developers, you can provide an email address to\n"
"let us ask more details about the bug. Additionally entering\n"
"a public PGP key enables us to encrypt such future\n"
"communication.</p>\n"
"<p>Anyone who can see this reply will probably infer you are\n"
"a Tails user. Time to wonder how much you trust your\n"
"Internet and mailbox providers?</p>\n"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:136
msgid "OpenPGP encryption applet"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:139
msgid "Exit"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:141
msgid "About"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:192
msgid "Encrypt Clipboard with _Passphrase"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:195
msgid "Sign/Encrypt Clipboard with Public _Keys"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:200
msgid "_Decrypt/Verify Clipboard"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:204
msgid "_Manage Keys"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:244
msgid "The clipboard does not contain valid input data."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:294
#: config/chroot_local-includes/usr/local/bin/gpgApplet:296
#: config/chroot_local-includes/usr/local/bin/gpgApplet:298
msgid "Unknown Trust"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:300
msgid "Marginal Trust"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:302
msgid "Full Trust"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:304
msgid "Ultimate Trust"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:357
msgid "Name"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:358
msgid "Key ID"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:359
msgid "Status"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:391
msgid "Fingerprint:"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:394
msgid "User ID:"
msgid_plural "User IDs:"
msgstr[0] ""
msgstr[1] ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:424
msgid "None (Don't sign)"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:487
msgid "Select recipients:"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:495
msgid "Hide recipients"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:498
msgid ""
"Hide the user IDs of all recipients of an encrypted message. Otherwise "
"anyone that sees the encrypted message can see who the recipients are."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:504
msgid "Sign message as:"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:508
msgid "Choose keys"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:548
msgid "Do you trust these keys?"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:551
msgid "The following selected key is not fully trusted:"
msgid_plural "The following selected keys are not fully trusted:"
msgstr[0] ""
msgstr[1] ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:569
msgid "Do you trust this key enough to use it anyway?"
msgid_plural "Do you trust these keys enough to use them anyway?"
msgstr[0] ""
msgstr[1] ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:582
msgid "No keys selected"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:584
msgid ""
"You must select a private key to sign the message, or some public keys to "
"encrypt the message, or both."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:612
msgid "No keys available"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:614
msgid ""
"You need a private key to sign messages or a public key to encrypt messages."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:742
msgid "GnuPG error"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:763
msgid "Therefore the operation cannot be performed."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:813
msgid "GnuPG results"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:819
msgid "Output of GnuPG:"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:844
msgid "Other messages provided by GnuPG:"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/shutdown_helper_applet:34
msgid "Shutdown Immediately"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/shutdown_helper_applet:35
msgid "Reboot Immediately"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/shutdown_helper_applet:72
msgid "Shutdown Helper"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-about:13
msgid "not available"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-about:16
#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:1
msgid "Tails"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-about:17
msgid "The Amnesic Incognito Live System"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-about:18
#, python-format
msgid ""
"Build information:\n"
"%s"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-about:20
msgid "About Tails"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:105
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:109
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:113
msgid "Your additional software"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:106
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:114
msgid ""
"The upgrade failed. This might be due to a network problem. Please check "
"your network connexion or try to restart Tails."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:110
msgid "The upgrade was successful."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-htp-notify-user:52
msgid "Synchronizing the system's clock"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-htp-notify-user:53
msgid ""
"Tor needs an accurate clock to work properly, especially for Hidden "
"Services. Please wait..."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-htp-notify-user:87
msgid "Failed to synchronize the clock!"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-security-check:86
#, perl-format
msgid "Unparseable line in %s"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-security-check:113
msgid "atom_str was passed an undefined argument"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-security-check:177
msgid "Empty fetched feed."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-security-check:194
msgid "This version of Tails has known security issues:"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-start-i2p:62
msgid "Starting I2P..."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-start-i2p:63
msgid "The I2P router console will be opened on start."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-start-i2p:82
#: config/chroot_local-includes/usr/local/bin/tails-start-i2p:124
msgid "I2P failed to start"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-start-i2p:83
msgid ""
"Make sure that you have a working Internet connection, then try to start I2P "
"again."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-start-i2p:125
msgid ""
"Something went wrong when I2P was starting. Look in the logs in the "
"following directory for more information:"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-virt-notify-user:53
msgid "Warning: virtual machine detected!"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-virt-notify-user:55
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-virt-notify-user:57
msgid ""
"<a href='file:///usr/share/doc/tails/website/doc/advanced_topics/"
"virtualization.en.html'>Learn more...</a>"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:59
msgid "error:"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:60
msgid "Error"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:69
msgid "warning:"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:70
msgid "Warning"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:79
msgid "Do you really want to launch the Unsafe Browser?"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:81
msgid ""
"Network activity within the Unsafe Browser is <b>not anonymous</b>. Only use "
"the Unsafe Browser if necessary, for example if you have to login or "
"register to activate your Internet connection."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:82
msgid "_Launch"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:83
msgid "_Exit"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:93
msgid "Starting the Unsafe Browser..."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:94
msgid "This may take a while, so please be patient."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:112
msgid "Failed to setup chroot."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:181
#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:1
msgid "Unsafe Browser"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:221
msgid "Shutting down the Unsafe Browser..."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:222
msgid ""
"This may take a while, and you may not restart the Unsafe Browser until it "
"is properly shut down."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:234
msgid "Failed to restart Tor."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:242
msgid ""
"Another Unsafe Browser is currently running, or being cleaned up. Please "
"retry in a while."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:255
msgid ""
"No DNS server was obtained through DHCP or manually configured in "
"NetworkManager."
msgstr ""

#: ../config/chroot_local-includes/etc/skel/Desktop/Report_a_Bug.desktop.in.h:1
msgid "Report a Bug"
msgstr ""

#: ../config/chroot_local-includes/etc/skel/Desktop/Tails_documentation.desktop.in.h:1
msgid "Tails documentation"
msgstr ""

#: ../config/chroot_local-includes/usr/share/applications/i2p.desktop.in.h:1
msgid "Anonymous overlay network "
msgstr ""

#: ../config/chroot_local-includes/usr/share/applications/i2p.desktop.in.h:2
msgid "i2p"
msgstr ""

#: ../config/chroot_local-includes/usr/share/applications/i2p.desktop.in.h:3
msgid "Anonymous overlay network"
msgstr ""

#: ../config/chroot_local-includes/usr/share/applications/tails-reboot.desktop.in.h:1
msgid "Reboot"
msgstr ""

#: ../config/chroot_local-includes/usr/share/applications/tails-reboot.desktop.in.h:2
msgid "Immediately reboot computer"
msgstr ""

#: ../config/chroot_local-includes/usr/share/applications/tails-shutdown.desktop.in.h:1
msgid "Power Off"
msgstr ""

#: ../config/chroot_local-includes/usr/share/applications/tails-shutdown.desktop.in.h:2
msgid "Immediately shut down computer"
msgstr ""

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:2
msgid "Browse the World Wide Web without anonymity"
msgstr ""

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:3
msgid "Unsafe Web Browser"
msgstr ""

#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:2
msgid "Tails specific tools"
msgstr ""
