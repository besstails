What a fscking mess:

http://www.securitytracker.com/id/1028590

Can we expect a Tails version 0.18.1 to fix this?

"Description: Multiple vulnerabilities were reported in X. A remote authenticated or local user can obtain elevated privileges on the target system.

Several X Window System client libraries do not properly validate data returned from an X server.

A remote authenticated or local user may be able to exploit this to cause arbitrary code to be executed by the target X client. If the X client runs with privileges, the user may be able to obtain those privileges."

##X.Org Security Advisory: May 23, 2013

Protocol handling issues in X Window System client libraries

http://www.x.org/wiki/Development/Security/Advisory-2013-05-23
