These questions all apply to browsing the internet from a computer that's tethered to a smartphone.

What is the difference between running the TOR app, versus the TOR windows program. 


I understand using a VPN defeats the anonymity of TOR. Does it make any difference if connecting to a VPN through the phone or computer?

Could a spare computer, router or smart phone(with no dataplan) be used to increase anonymity on the internet in any way? 

Thanks guys, just some things I've been curious about for a while.
