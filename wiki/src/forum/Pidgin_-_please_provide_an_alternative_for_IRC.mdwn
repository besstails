or state on a wiki or page somewhere on your site on how to configure another IRC client if you don't include another in the future.

One of many examples:

[Likewise, when discussing security vulnerabilities... VLC doesn't have
the best track record.  (See https://www.videolan.org/security/ ).
I'm a big fan of VLC, but I put it in the same category as Pidgin when
it comes to "how far do I trust this program to not have bugs?"]

https://lists.torproject.org/pipermail/tor-talk/2013-May/028150.html
