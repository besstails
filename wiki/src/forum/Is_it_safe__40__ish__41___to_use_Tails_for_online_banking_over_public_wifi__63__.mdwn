I've looked, I swear, but I can't find a clear answer suitable for a n00b. I hope a local expert here can help! 

The situation is this: I'm going to be travelling, and I'm going to have to make a few payments via online banking while on the road. The only connection I'm likely to have is public wifi. 

I realize nothing is 100% safe, there are man-in-the-middle possibilities, etc., etc., etc. I'm not too worried about that. I'd just like something safer than doing that stuff right out in the open, which happened to me the last time I was travelling and had a banking issue come up. (Luckily, no ill effects after that. :) )

So, if I use Tails OS on a bootable USB, am I reasonably likely not to get my details stolen? Or have I misunderstood how Tails works?
