[[!meta title="Contributors meetings"]]

Each month a contributors meeting is organized. It currently happens on
**the first Thursday of the month on [[#tails-dev|contribute/chat]] at
8pm UTC (10pm CEST)**. Every Tails contributor is welcome to attend.

A proposed agenda is sent a few days in advance on tails-dev@boum.org.
To propose and prepare other discussion topics, raise them them in
advance on tails-dev@boum.org, so that others can help prepare the
discussion as well.

If you want to get involved but don't know yet how, please introduce
yourself during the meeting, and be sure to tell us what you are
interested in.

The meeting might not be the most adequate time and place to properly
introduce newcomers to the development process, but at least it should
be a fine place to know each others, and schedule a better suited event.
