# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: ACKAGE VERSION\n"
"POT-Creation-Date: 2013-05-24 23:20+0300\n"
"PO-Revision-Date: 2013-05-10 12:18-0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Manually installing onto a USB stick, for Mac\"]]\n"
msgstr "[[!meta title=\"Installer manuellement sur une clé USB, depuis Mac\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/first_steps/manual_usb_installation.intro\" raw=\"yes\"]]\n"
msgstr "[[!inline pages=\"doc/first_steps/manual_usb_installation.intro.fr\" raw=\"yes\"]]\n"

#. type: Plain text
msgid "This technique uses the command line."
msgstr "Cette méthode utilise les lignes de commandes."

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h2 class=\"bullet-number-one\">Setup rEFInd</h2>\n"
msgstr "<h2 class=\"bullet-number-one\">Configurer rEFInd</h2>\n"

#. type: Plain text
msgid ""
"You need to have [rEFInd](http://sourceforge.net/projects/refind/) installed "
"and working on the Mac."
msgstr ""
"Vous devez avoir [rEFInd](http://sourceforge.net/projects/refind/) installé "
"et fonctionnel sur le Mac."

#. type: Plain text
msgid ""
"If you need help with rEFInd, look at [their installation documentation]"
"(http://www.rodsbooks.com/refind/installing.html)."
msgstr ""
"Si vous avez besoin d'aide concernant rEFInd, jetez un œil à [leur "
"documentation d'installation (en anglais)](http://www.rodsbooks.com/refind/"
"installing.html)."

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"caution\">\n"
"rEFInd will replace your original bootloader.<br/>\n"
"<strong>This could cause your Mac to not boot.</strong> It is recommended to create a full backup and know how to\n"
"restore. See <a href=\"https://support.apple.com/kb/HT1427\">Apple's\n"
"instructions</a>.\n"
"</div>\n"
msgstr ""
"<div class=\"caution\">\n"
"rEFInd remplacera votre programme d'amorçage par défaut.<br/>\n"
"<strong>Cela peut empêcher votre Mac de démarrer.</strong> Il est recommandé de faire une sauvegarde complète ainsi\n"
"que de savoir comment le restaurer. Voir <a href=\"https://support.apple.com/kb/HT1427?viewlocale=fr_FR\">\n"
"les instructions d'Apple</a>.\n"
"</div>\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"step_2\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h2 class=\"bullet-number-two\">Find out the device name of the USB stick</h2>\n"
msgstr "<h2 class=\"bullet-number-two\">Trouver le nom de périphérique de la clé USB</h2>\n"

#. type: Plain text
msgid ""
"The device name should be something like `/dev/disk1`, `/dev/disk2`, etc."
msgstr ""
"Le nom du périphérique devrait être quelque chose comme `/dev/disk1`, `/dev/"
"disk2`, etc."

#. type: Plain text
msgid "If you are not sure about the exact device name, do the following:"
msgstr ""
"Si vous n'êtes pas certain du son nom exact du périphérique, procédez comme "
"suit :"

#. type: Bullet: '  1. '
msgid "Unplug the USB stick."
msgstr "Débranchez la clé USB."

#. type: Plain text
#, no-wrap
msgid ""
"  1. Open <span class=\"application\">Terminal</span> from\n"
"     <span class=\"menuchoice\">\n"
"       <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"       <span class=\"guisubmenu\">Utilities</span>&nbsp;▸\n"
"       <span class=\"guimenuitem\">Terminal.app</span>\n"
"     </span>\n"
"  1. Execute the following command:\n"
msgstr ""
"  1. Ouvrir un <span class=\"application\">Terminal</span> depuis\n"
"     <span class=\"menuchoice\">\n"
"       <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"       <span class=\"guisubmenu\">Utilitaires</span>&nbsp;▸\n"
"       <span class=\"guimenuitem\">Terminal.app</span>\n"
"     </span>\n"
"  1. Exécutez la commande suivante :\n"

#. type: Plain text
#, no-wrap
msgid "         diskUtil list\n"
msgstr "         diskUtil list\n"

#. type: Plain text
#, no-wrap
msgid "     This returns a list of all the current storage devices. For example:\n"
msgstr "     Ceci renvoi la liste des périphériques de stockages actuels. Par exemple :\n"

#. type: Plain text
#, no-wrap
msgid ""
"<pre>\n"
"$ diskUtil list\n"
"/dev/disk0\n"
"    #:                       TYPE NAME                 SIZE       IDENTIFIER\n"
"    0:      GUID_partition_scheme                     *500.1 GB   disk0\n"
"    1:                        EFI                      209.7 MB   disk0s1\n"
"    2:                  Apple_HFS MacDrive             250.0 GB   disk0s2\n"
"    3:                        EFI                      134.1 GB   disk0s3\n"
"    4:       Microsoft Basic Data BOOTCAMP             115.5 GB   disk0s4\n"
"</pre>\n"
msgstr ""
"<pre>\n"
"$ diskUtil list\n"
"/dev/disk0\n"
"    #:                       TYPE NAME                 SIZE       IDENTIFIER\n"
"    0:      GUID_partition_scheme                     *500.1 GB   disk0\n"
"    1:                        EFI                      209.7 MB   disk0s1\n"
"    2:                  Apple_HFS MacDrive             250.0 GB   disk0s2\n"
"    3:                        EFI                      134.1 GB   disk0s3\n"
"    4:       Microsoft Basic Data BOOTCAMP             115.5 GB   disk0s4\n"
"</pre>\n"

#. type: Bullet: '  1. '
msgid "Plug back the USB stick and run the same command as before:"
msgstr "Rebranchez la clé USB et lancez la même commande que précédemment :"

#. type: Plain text
#, no-wrap
msgid ""
"     A new device should appear in the list of storage devices. Check\n"
"     that the size of the device corresponds to the size of your USB\n"
"     stick.\n"
msgstr ""
"     Un nouveau périphérique devrait apparaître dans la liste des périphériques\n"
"     de stockage. Vérifier que la taille du périphérique correspond à la taille de\n"
"     votre clé USB.\n"

#. type: Plain text
#, no-wrap
msgid ""
"<pre>\n"
"$ diskUtil list\n"
"/dev/disk0\n"
"   #:                       TYPE NAME                 SIZE       IDENTIFIER\n"
"   0:      GUID_partition_scheme                     *500.1 GB   disk0\n"
"   1:                        EFI                      209.7 MB   disk0s1\n"
"   2:                  Apple_HFS MacDrive             250.0 GB   disk0s2\n"
"   3:                        EFI                      134.1 GB   disk0s3\n"
"   4:       Microsoft Basic Data BOOTCAMP             115.5 GB   disk0s4\n"
"/dev/disk1\n"
"   #:                       TYPE NAME                 SIZE       IDENTIFIER\n"
"   0:     FDisk_partition_scheme                     *4.0 GB     disk1\n"
"   1:                  Apple_HFS Untitled 1           4.0 GB     disk1s1\n"
"</pre>\n"
msgstr ""
"<pre>\n"
"$ diskUtil list\n"
"/dev/disk0\n"
"   #:                       TYPE NAME                 SIZE       IDENTIFIER\n"
"   0:      GUID_partition_scheme                     *500.1 GB   disk0\n"
"   1:                        EFI                      209.7 MB   disk0s1\n"
"   2:                  Apple_HFS MacDrive             250.0 GB   disk0s2\n"
"   3:                        EFI                      134.1 GB   disk0s3\n"
"   4:       Microsoft Basic Data BOOTCAMP             115.5 GB   disk0s4\n"
"/dev/disk1\n"
"   #:                       TYPE NAME                 SIZE       IDENTIFIER\n"
"   0:     FDisk_partition_scheme                     *4.0 GB     disk1\n"
"   1:                  Apple_HFS Untitled 1           4.0 GB     disk1s1\n"
"</pre>\n"

#. type: Plain text
msgid ""
"In this example, the USB stick is 4.0 GB and the device name is `/dev/"
"disk1`.  Yours are probably different."
msgstr ""
"Dans cet exemple, la clé USB fait 4.0 GB et le nom du périphérique est `/dev/"
"disk1`. La vôtre est probablement différente."

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"caution\">\n"
"If you are not sure about the device name you should stop proceeding or\n"
"<strong>you risk overwriting any hard drive on the system</strong>.\n"
"</div>\n"
msgstr ""
"<div class=\"caution\">\n"
"Si vous n'êtes pas sûr du nom du périphérique vous devriez arrêtez la\n"
"manipulation ou <strong>vous risquez d'écraser un disque dur du système</strong>.\n"
"</div>\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"step_3\"></a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h2 class=\"bullet-number-three\">Unmount the USB drive</h2>\n"
msgstr "<h2 class=\"bullet-number-three\">Démonter la clé USB</h2>\n"

#. type: Plain text
msgid ""
"Execute the following command, replacing `[device]` with the device name "
"found in step 2."
msgstr ""
"Exécutez la commande suivante, en remplaçant `[device]` par le nom du "
"périphérique trouvé à l'étape 2."

#. type: Plain text
#, no-wrap
msgid "     diskUtil unmountDisk [device]\n"
msgstr "     diskUtil unmountDisk [device]\n"

#. type: Plain text
#, no-wrap
msgid "<h2 class=\"bullet-number-four\">Run isohybrid.pl on the ISO image</h2>\n"
msgstr "<h2 class=\"bullet-number-four\">Lancer isohybrid.pl sur l'image ISO</h2>\n"

#. type: Plain text
msgid ""
"You need to modify the ISO image using `isohybrid` before copying it onto "
"the USB stick."
msgstr ""
"Vous devez modifier l'image ISO en utilisant `isohybrid` avant de la copier "
"sur la clé USB."

#. type: Bullet: '1. '
msgid ""
"Download [syslinux](http://ftp.debian.org/debian/pool/main/s/syslinux/"
"syslinux_4.02+dfsg.orig.tar.gz)."
msgstr ""
"Téléchargez [syslinux](http://ftp.debian.org/debian/pool/main/s/syslinux/"
"syslinux_4.02+dfsg.orig.tar.gz)."

#. type: Bullet: '1. '
msgid "Double click on the package to extract it."
msgstr "Double cliquez sur le paquet pour l'extraire."

#. type: Bullet: '1. '
msgid "Copy `isohybrid.pl` from the `/utils` folder to the desktop."
msgstr "Copiez `isohybrid.pl`depuis le dossier `/utils` sur le bureau."

#. type: Bullet: '1. '
msgid ""
"Copy the ISO image (for example `tails-i386-0.17.1.iso`) to the desktop."
msgstr ""
"Copiez l'image ISO (par exemple `tails-i386-0.17.1.iso`) sur le bureau."

#. type: Bullet: '1. '
msgid "To change directory into the desktop, execute:"
msgstr "Pour changer de dossier et aller sur le bureau, faire :"

#. type: Plain text
#, no-wrap
msgid "       cd Desktop\n"
msgstr "       cd Desktop\n"

#. type: Bullet: '1. '
msgid ""
"To run `isohybrid.pl` on the ISO image, execute the following command, "
"replacing `[tails.iso]` with the path to the ISO image that you want to "
"install."
msgstr ""
"Pour lancer `isohybrid.pl` sur l'image ISO, exécutez la commande suivante, "
"en remplaçant `[tails.iso]` par le chemin de l'image ISO que vous voulez "
"installer."

#. type: Plain text
#, no-wrap
msgid "       perl isohybrid.pl [tails.iso]\n"
msgstr "       perl isohybrid.pl [tails.iso]\n"

#. type: Plain text
#, no-wrap
msgid "   Here is an example of the commands to execute, yours are probably different:\n"
msgstr "   Voici un exemple de commande à exécuter, la vôtre est probablement différente :\n"

#. type: Plain text
#, no-wrap
msgid "       perl isohybrid.pl tails-i386-0.17.1.iso\n"
msgstr "       perl isohybrid.pl tails-i386-0.17.1.iso\n"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"tip\">\n"
"If you are not sure about the path to the ISO image or if you get a\n"
"<span class=\"guilabel\">No such\n"
"file or directory</span> error, you can first type `perl isohybrid.pl`, followed by a space, and\n"
"then drag and drop the icon of the ISO image from a file browser onto\n"
"<span class=\"application\">\n"
"Terminal</span>. This should insert the correct path to the ISO image in\n"
"<span class=\"application\">Terminal</span>.\n"
"Then complete the command and execute it.\n"
"</div>\n"
msgstr ""
"<div class=\"tip\">\n"
"Si vous n'êtes pas sûr du chemin menant à votre image ISO ou si vous\n"
"obtenez une erreur du type <span class=\"guilabel\">No such file or directory</span>,\n"
"vous pouvez d'abord taper, dans le terminal,  `perl isohybrid.pl`, suivi d'un espace, et\n"
"venir glisser-déposer l'icône de votre image ISO depuis le navigateur de fichiers vers\n"
"le <span class=\"application\">\n"
"Terminal</span>. Cela devrait insérer le chemin correct de l'image ISO dans le\n"
"<span class=\"application\">Terminal</span>.\n"
"Complétez ensuite la commande et exécutez-la.\n"
"</div>\n"

#. type: Plain text
#, no-wrap
msgid "<h2 class=\"bullet-number-five\">Do the copy</h2>\n"
msgstr "<h2 class=\"bullet-number-five\">Faire la copie</h2>\n"

#. type: Plain text
msgid ""
"Execute the following command, replacing `[tails.iso]` by the path to the "
"ISO image that you want to copy and `[device]` by the device name found in "
"step 1."
msgstr ""
"Exécutez la commande suivante, en remplaçant `[tails.iso]` par le chemin de "
"l'image ISO que vous voulez copier et `[device]` par le nom du périphérique "
"trouvé à l'étape 1."

#. type: Plain text
#, no-wrap
msgid "    cat [tails.iso] > [device] && sync\n"
msgstr "    cat [tails.iso] > [device] && sync\n"

#. type: Plain text
msgid "You should get something like this:"
msgstr "Vous devriez obtenir quelque chose comme :"

#. type: Plain text
#, no-wrap
msgid "    cat tails-i386-0.17.1.iso > /dev/disk1 && sync\n"
msgstr "    cat tails-i386-0.17.1.iso > /dev/disk1 && sync\n"

#. type: Plain text
msgid ""
"If you don't see any error message, Tails is being copied onto the USB "
"stick. The whole process might take some time, generally a few minutes."
msgstr ""
"Si vous ne voyez aucun message d'erreur, Tails est en train d'être copié "
"vers la clé USB. Ce processus peut prendre du temps, généralement quelques "
"minutes."

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"next\">\n"
"<p>Once the command prompt reappears, you can restart your Mac.\n"
"Wait for the rEFInd menu and select the USB stick to\n"
"[[start Tails|/download/#start]].</p>\n"
"</div>\n"
msgstr ""
"<div class=\"next\">\n"
"<p>Lorsque l'invite de commande réapparaît, vous pouvez redémarrer.\n"
"votre Mac. Attendre le menu rEFInd et sélectionnez la clé USB pou\n"
"[[démarrer Tails|/download/#start]].</p>\n"
"</div>\n"

#. type: Title =
#, no-wrap
msgid "Notes\n"
msgstr "Notes\n"

#. type: Plain text
msgid "This method was successfully tested on the following hardware:"
msgstr "Cette méthode à été testée avec succès sur le matériel suivant :"

#. type: Bullet: '  - '
msgid "MacBook Pro Model A1150 with OS X 10.6.8, 2006"
msgstr "MacBook Pro Model A1150 avec OS X 10.6.8, 2006"

#. type: Plain text
msgid ""
"The method worked on some hardware but a bug in the video support prevented "
"Tails to start successfully:"
msgstr ""
"Cette méthode marchait sur d'autre matériel mais un bug dans la prise en "
"charge vidéo empêchait Tails de finir de démarrer :"

#. type: Bullet: '  - '
msgid "MacBook Pro Retina with OS X 10.8.3, December 2012"
msgstr "MacBook Pro Retina avec OS X 10.8.3, décembre 2012"

#. type: Bullet: '  - '
#, fuzzy
#| msgid "MacBook Pro Model A1150 with OS X 10.6.8, 2006"
msgid "Macbook Pro model A1150"
msgstr "MacBook Pro Model A1150 avec OS X 10.6.8, 2006"

#. type: Plain text
msgid ""
"Note that Tails developers are in general not very knowledgeable about Mac. "
"Any additional information is welcome."
msgstr ""
"Veuillez noter que les développeurs de Tails ne s'y connaissent généralement "
"pas bien en Mac. Toute information supplémentaire est la bienvenue."

#. type: Plain text
#, no-wrap
msgid ""
"<!--\n"
"An alternative method was suggested:\n"
msgstr ""
"<!--\n"
"Une méthode alternative a été suggérée :\n"

#. type: Bullet: '1. '
msgid "Open Disk Utility"
msgstr "Ouvrir Disk Utility"

#. type: Bullet: '1. '
msgid "Find The Drive"
msgstr "Trouver la clé"

#. type: Bullet: '1. '
msgid "Format Tab"
msgstr "La formater"

#. type: Bullet: '1. '
msgid "Source is ISO File"
msgstr "La source est le fichier ISO"

#. type: Plain text
#, no-wrap
msgid ""
"1. Destination is USB Drive\n"
"-->\n"
msgstr ""
"1. La destination est la clé USB\n"
"-->\n"
