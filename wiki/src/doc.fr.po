# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: SACKAGE VERSION\n"
"POT-Creation-Date: 2013-04-09 17:38+0300\n"
"PO-Revision-Date: 2013-04-07 11:06-0000\n"
"Last-Translator: amnesia <amnesia@boum.org>\n"
"Language-Team: SLANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Documentation\"]]\n"
msgstr "[[!meta title=\"Documentation\"]]\n"

#. type: Plain text
msgid "This documentation is a work in progress and a collective task."
msgstr ""
"Cette documentation est en perpétuelle évolution, ainsi qu'un travail "
"collectif."

#. type: Plain text
msgid ""
"Read about how you can help [[improving Tails documentation|/contribute/how/"
"documentation]]."
msgstr ""
"Si vous souhaitez participer, [[lisez la documentation correspondante|/"
"contribute/how/documentation]]."

#. type: Plain text
msgid "- [[Introduction to this documentation|introduction]]"
msgstr "- [[Introduction sur cette documentation|introduction]]"

#. type: Title #
#, no-wrap
msgid "[[General information|about]]"
msgstr "[[Informations générales|about]]"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/about.index\" raw=\"yes\"]]\n"
msgstr "[[!inline pages=\"doc/about.index.fr\" raw=\"yes\"]]\n"

#. type: Title #
#, no-wrap
msgid "[[Get Tails|download]]"
msgstr "[[Télécharger Tails|download]]"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/get.index\" raw=\"yes\"]]\n"
msgstr "[[!inline pages=\"doc/get.index.fr\" raw=\"yes\"]]\n"

#. type: Title #
#, no-wrap
msgid "[[First steps with Tails|first_steps]]"
msgstr "[[Premier pas avec Tails|first_steps]]"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/first_steps.index\" raw=\"yes\"]]\n"
msgstr "[[!inline pages=\"doc/first_steps.index.fr\" raw=\"yes\"]]\n"

#. type: Title #
#, no-wrap
msgid "[[Connect to the Internet anonymously|anonymous_internet]]"
msgstr "[[Se connecter à internet anonymement|anonymous_internet]]"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/anonymous_internet.index\" raw=\"yes\"]]\n"
msgstr "[[!inline pages=\"doc/anonymous_internet.index.fr\" raw=\"yes\"]]\n"

#. type: Title #
#, no-wrap
msgid "[[Encryption & privacy|encryption_and_privacy]]"
msgstr "[[Chiffrement et vie privée|encryption_and_privacy]]"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/encryption_and_privacy.index\" raw=\"yes\"]]\n"
msgstr "[[!inline pages=\"doc/encryption_and_privacy.index.fr\" raw=\"yes\"]]\n"

#. type: Title #
#, no-wrap
msgid "[[Work on sensitive documents|sensitive_documents]]"
msgstr "[[Travailler sur des documents sensibles|sensitive_documents]]"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/sensitive_documents.index\" raw=\"yes\"]]\n"
msgstr "[[!inline pages=\"doc/sensitive_documents.index.fr\" raw=\"yes\"]]\n"

#. type: Title #
#, no-wrap
msgid "[[Advanced topics|advanced_topics]]"
msgstr "[[Sujets avancés|advanced_topics]]"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/advanced_topics.index\" raw=\"yes\"]]\n"
msgstr "[[!inline pages=\"doc/advanced_topics.index.fr\" raw=\"yes\"]]\n"

#~ msgid ""
#~ "This documentation is a work in progress and a collective task. If you "
#~ "think it lacks documentation on a specific topic you can suggest us to "
#~ "complete it or try to write it yourself and share it with us."
#~ msgstr ""
#~ "Cette documentation est en perpétuelle évolution ainsi qu'un travail "
#~ "collectif. Si vous pensez qu'il y a des manques sur un sujet spécifique, "
#~ "vous pouvez nous suggérer de la compléter ou essayer de le faire par vous-"
#~ "même et partager le résultat avec nous."
